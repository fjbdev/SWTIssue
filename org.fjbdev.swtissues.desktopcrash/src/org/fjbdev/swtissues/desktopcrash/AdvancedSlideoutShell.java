package org.fjbdev.swtissues.desktopcrash;


import org.eclipse.jface.window.ToolTip;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;

/**
 * Part of this tooltip is copied from {@link ToolTip}.
 */
public abstract class AdvancedSlideoutShell {

   private ToolTipAllControlsListener _ttAllControlsListener;

   private Point                      _shellStartLocation;
   private Point                      _shellEndLocation                   = new Point(0, 0);

   protected boolean                  isShellMoved;

   private AbstractRRShell            _visibleRRShell;
   private AbstractRRShell            _rrShellWithResize;
   private AbstractRRShell            _rrShellNoResize;

   private Composite _ttContentArea;

   /**
    * Tooltip shell which is currently be visible
    */
   private Shell     _visibleShell;

   /**
    * The control on whose action the tooltip is shown
    */
   private Control   _ownerControl;

   public class RRShell extends AbstractRRShell {

      public RRShell(final Shell parentShell, final int style, final String shellTitle) {
         super(parentShell, style, shellTitle);
      }

      @Override
      public Point getContentSize() {
         return   new Point(200, 50);
      }
   }

   /**
    * This listener is added to ALL widgets within the tooltip shell.
    */
   private class ToolTipAllControlsListener implements Listener {
      @Override
      public void handleEvent(final Event event) {
         onTTAllControlsEvent(event);
      }

   }

   public AdvancedSlideoutShell(final Control ownerControl) {

      _ownerControl = ownerControl;

      _ttAllControlsListener = new ToolTipAllControlsListener();
   }


   private void animation10_Start() {

     
      animation10_StartKomplex();
   }

   private void animation10_StartKomplex() {

            // set fading in location


               // shell is not visible, set position directly without moving animation, do only fading animation

               _shellStartLocation = _shellEndLocation;

               _visibleRRShell.setShellLocation(_shellStartLocation.x, _shellStartLocation.y, 2);

               reparentShell(_rrShellNoResize);

               setShellVisible(true);

   }


   /**
    * This is called just before the shell is set to be visible or hidden
    *
    * @param isVisible
    *           Is <code>true</code> when visible otherwise hidden.
    */
   protected void beforeShellVisible(final boolean isVisible) {}

   /**
    * Creates the content area of the the tooltip.
    *
    * @param parent
    *           the parent of the content area
    * @return the content area created
    */
   protected abstract Composite createSlideoutContentArea(Composite parent);

   /**
    * Create a shell but do not display it
    *
    * @return Returns <code>true</code> when shell is created.
    */
   private void createUI() {

      /*
       * resize shell
       */
      _rrShellWithResize = new RRShell(
            _ownerControl.getShell(), //
            SWT.ON_TOP //
                  //                  | SWT.TOOL
                  | SWT.RESIZE
                  | SWT.NO_FOCUS,
            "");

      /*
       * no resize shell
       */
      _rrShellNoResize = new RRShell(
            _ownerControl.getShell(), //
            SWT.ON_TOP //
                  //                  | SWT.TOOL
                  | SWT.NO_FOCUS,
           "");

      setCurrentVisibleShell(_rrShellNoResize);

      // set initial alpha
      _visibleShell.setAlpha(0x0);


      final Composite container = new Composite(_ownerControl.getShell(), SWT.NONE);
    		container.setLayout(new FillLayout());
          // create UI
          _ttContentArea = createSlideoutContentArea(container);


      ttAllControlsAddListener(_visibleShell);

   }

   private void onTTAllControlsEvent(final Event event) {


      switch (event.type) {

      case SWT.MouseEnter:

//         System.out.println(UI.timeStampNano() + " _isDoNotStopAnimation " + _isDoNotStopAnimation);
//         // TODO remove SYSTEM.OUT.PRINTLN

         reparentShell(_rrShellWithResize);

         break;}

     
   }

   /**
    * Reparent shell
    *
    * @param newReparentedShell
    *           Shell which should be used to display {@link #_ttContentArea}.
    */
   private void reparentShell(final AbstractRRShell newReparentedShell) {

      if (_visibleShell == newReparentedShell.getShell()) {
         // shell is already visible
         return;
      }

      final Shell prevShell = _visibleShell;

      ttAllControlsRemoveListener(prevShell);

      setCurrentVisibleShell(newReparentedShell);

      int trimDiffX = _rrShellWithResize.getShellTrimWidth() - _rrShellNoResize.getShellTrimWidth();
      int trimDiffY = _rrShellWithResize.getShellTrimHeight() - _rrShellNoResize.getShellTrimHeight();

      if (newReparentedShell == _rrShellWithResize) {

         // setup resize shell

         _rrShellWithResize.reparentFromOtherShell(_rrShellNoResize, _ttContentArea);

         trimDiffX -= trimDiffX;
         trimDiffY -= trimDiffY;

      }

      // hide previous shell
      prevShell.setVisible(false);

      ttAllControlsAddListener(newReparentedShell.getShell());

   }


   /**
    * Set shell which is currently be visible.
    *
    * @param rrShell
    */
   private void setCurrentVisibleShell(final AbstractRRShell rrShell) {

      _visibleRRShell = rrShell;
      _visibleShell = rrShell.getShell();
   }

   private void setShellVisible(final boolean isVisible) {

      beforeShellVisible(isVisible);

      _visibleShell.setVisible(isVisible);

   }

   protected boolean showShell() {

      createUI();

      ttShow();

      return true;
   }

   /**
    * ########################### Recursive #########################################<br>
    * <p>
    * Add listener to all controls within the tooltip
    * <p>
    * ########################### Recursive #########################################<br>
    *
    * @param control
    */
   private void ttAllControlsAddListener(final Control control) {

      control.addListener(SWT.MouseEnter, _ttAllControlsListener);

      if (control instanceof Composite) {
         final Control[] children = ((Composite) control).getChildren();
         for (final Control child : children) {
            ttAllControlsAddListener(child);
         }
      }
   }

   /**
    * ########################### Recursive #########################################<br>
    * <p>
    * Removes listener from all controls within the tooltip
    * <p>
    * ########################### Recursive #########################################<br>
    *
    * @param control
    */
   private void ttAllControlsRemoveListener(final Control control) {

      control.removeListener(SWT.MouseEnter, _ttAllControlsListener);

      if (control instanceof Composite) {
         final Control[] children = ((Composite) control).getChildren();
         for (final Control child : children) {
            ttAllControlsRemoveListener(child);
         }
      }
   }


   private void ttShow() {

      // shell is not yet fading in

      animation10_Start();
   }

}
