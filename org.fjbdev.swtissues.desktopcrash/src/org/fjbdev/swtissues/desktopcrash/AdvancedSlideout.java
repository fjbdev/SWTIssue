package org.fjbdev.swtissues.desktopcrash;

import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/*
 * Resize control: org.eclipse.jface.text.AbstractInformationControl
 */
public abstract class AdvancedSlideout extends AdvancedSlideoutShell {


   public AdvancedSlideout(final Control ownerControl) {

      super(ownerControl);

      initUI(ownerControl);

   }


   protected abstract void createSlideoutContent(Composite parent);

   @Override
   protected Composite createSlideoutContentArea(final Composite parent) {

      final Composite container = createUI(parent);

      return container;
   }

   private Composite createUI(final Composite parent) {

      final Composite shellContainer = new Composite(parent, SWT.NONE);
      GridLayoutFactory
            .swtDefaults() //
            .spacing(0, 0)
            .applyTo(shellContainer);
      {
         createSlideoutContent(shellContainer);
      }


      return shellContainer;
   }


   private void initUI(final Control ownerControl) {

   }

   public void open(final boolean isOpenDelayed) {
         showShell();
   }
}
