package org.fjbdev.swtissues.desktopcrash;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.part.PageBook;


public abstract class AbstractRRShell {

	private int			_shellTrimWidth;
	private int			_shellTrimHeight;

	/*
	 * UI resources
	 */
	private Display		_display;

	private Shell		_shell;

	private PageBook	_shellBook;
	private Composite	_pageShell;
	private Composite	_pageReparentableImage;

	private Image		_otherShellImage;

	public AbstractRRShell(final Shell parentShell, final int style, final String shellTitle) {

		_display = parentShell.getDisplay();

		_shell = new Shell(parentShell, style);
		_shell.setLayout(new FillLayout());

		_shell.setText(shellTitle);

		setTrimSize();

		setContentSize(getContentSize());

//		_shell.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));

		_shellBook = new PageBook(_shell, SWT.NONE);
		{
			_pageShell = createUI_10_PageShellContent(_shellBook);
			_pageReparentableImage = createUI_20_PageShellImage(_shellBook);
		}

		_shellBook.showPage(_pageShell);
	}

	private Composite createUI_10_PageShellContent(final Composite parent) {

		final Composite container = new Composite(parent, SWT.NONE);
		container.setLayout(new FillLayout());


		return container;
	}

	private Composite createUI_20_PageShellImage(final Composite parent) {

		final Canvas resizeCanvas = new Canvas(//
				parent,
				SWT.NONE //
		);

		resizeCanvas.setLayout(new FillLayout());

		return resizeCanvas;
	}

	public abstract Point getContentSize();

	public Shell getShell() {
		return _shell;
	}

	public int getShellTrimHeight() {
		return _shellTrimHeight;
	}

	public int getShellTrimWidth() {
		return _shellTrimWidth;
	}

	public void reparentFromOtherShell(final AbstractRRShell otherRRShell, final Composite reparentContainer) {

		final Shell otherShell = otherRRShell.getShell();

		final Rectangle otherShellBounds = otherShell.getBounds();
		final Rectangle otherClientAreaBounds = otherShell.getClientArea();

		/*
		 * copy NoResize shell image into the resize shell, to prevent flickering
		 */
		_otherShellImage = new Image(_display, otherClientAreaBounds);

		final GC gc = new GC(otherShell);
		gc.copyArea(_otherShellImage, 0, 0);
		gc.dispose();

		_shellBook.showPage(_pageReparentableImage);

		/*
		 * set shell position with new trim size so that the shell content is not moving
		 */
		final int trimDiffX = _shellTrimWidth - otherRRShell._shellTrimWidth;
		final int trimDiffY = _shellTrimHeight - otherRRShell._shellTrimHeight;

		final int shellX = otherShellBounds.x - trimDiffX;
		final int shellY = otherShellBounds.y - trimDiffY;

		_shell.setLocation(shellX, shellY);

		_shell.setAlpha(0x0);

		/*
		 * this will paint the shell image before reparenting takes place
		 */
		_shell.setVisible(true);

		_shell.setAlpha(0xff);

		otherRRShell.setAlpha(0);

		// reparent UI container
		reparentContainer.setParent(_pageShell);

		_shellBook.showPage(_pageShell);

		_otherShellImage.dispose();
	}

	public void setAlpha(final int alpha) {
		_shell.setAlpha(alpha);
	}

	public void setContentSize(final int width, final int height) {

		final int shellWidth = width + _shellTrimWidth * 2;
		final int shellHeight = height + _shellTrimHeight * 2;

		_shell.setSize(shellWidth, shellHeight);
	}

	private void setContentSize(final Point size) {
		setContentSize(size.x, size.y);
	}

	public void setShellLocation(final int x, final int y, final int flag) {
		_shell.setLocation(x, y);
	}

	private void setTrimSize() {

		final Point contentSize = getContentSize();

		final int contentWidth = contentSize.x;
		final int contentHeight = contentSize.y;

		final Rectangle contentWithTrim = _shell.computeTrim(0, 0, contentWidth, contentHeight);

		final int shellTrimWidth = contentWithTrim.width - contentWidth;
		final int shellTrimHeight = contentWithTrim.height - contentHeight;

		_shellTrimWidth = shellTrimWidth / 2;
		_shellTrimHeight = shellTrimHeight / 2;
	}

	public void updateColors(final Color fgColor, final Color bgColor) {

		_shell.setBackground(bgColor);
	}

}
