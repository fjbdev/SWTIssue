package org.fjbdev.swtissues.desktopcrash;

import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.dialogs.MessageDialog;

import org.eclipse.swt.SWT;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

import org.eclipse.swt.widgets.ToolItem;


public class LinuxDesktopCrasher extends AdvancedSlideout  {

   public LinuxDesktopCrasher(final ToolItem toolItem,
                                final IDialogSettings state) {

      super( toolItem.getParent());
   }

   @Override
   protected void createSlideoutContent(final Composite parent) {

	   
	   Button      button = new Button(parent, SWT.PUSH);
       button.setText("Ready to crash the linux desktop ?");
       button.addSelectionListener(new SelectionAdapter() {
          @Override
          public void widgetSelected(final SelectionEvent e) {
        	  /*
               * Confirm the crash
               */
             
                 MessageDialog.openConfirm(
                       Display.getCurrent().getActiveShell(),
                       "Make my Linux desktop crash",
                       "too late...");
          }
       });
       button.setEnabled(true);
   }
}
