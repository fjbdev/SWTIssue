package org.fjbdev.swtissues.desktopcrash;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.model.application.ui.menu.MToolItem;
import org.eclipse.swt.widgets.ToolItem;
 
public class OpenHandler {
	@Execute
	public void execute(MToolItem mToolItem) {
		 ToolItem toolItem = (ToolItem) mToolItem.getWidget();
		 AdvancedSlideout  _slideoutTourTagFilter = new LinuxDesktopCrasher(toolItem, null);
	      _slideoutTourTagFilter.open(false);	
   }
}