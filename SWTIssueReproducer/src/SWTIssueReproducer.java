
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.layout.PixelConverter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.widgets.Shell;

public class SWTIssueReproducer {

	private static Button     _btnLoadFile;
	private static Scale _scaleImageQuality;
	private static Label _labelImageQualityValue;
	private static Label _labelImageQuality;
	private static Button _btnCompressFile;
	private static String jpegAbsolutePath;
   public static void main(final String[] args) {
      InitUI();
   }

   public static Display getDisplay() {
	      Display display = Display.getCurrent();
	      //may be null if outside the UI thread
	      if (display == null) {
	         display = Display.getDefault();
	      }
	      return display;
	   }
   
   static void InitUI() {
      final Display display = new Display();
      final Shell shell = new Shell();
      shell.setSize(300, 300);
      shell.setLayout(new GridLayout());

    createUI(shell.getShell());

      shell.setText("SWT Demo");
      shell.setSize(500,200);
      shell.open();

      while (!shell.isDisposed()) {
         if (!display.readAndDispatch()) {
            display.sleep();
         }
      }
   }
   
   private static void createUI(final Composite parent) {
	   PixelConverter _pc = new PixelConverter(parent);
	      final Composite shellContainer = new Composite(parent, SWT.NONE);
	      GridDataFactory.fillDefaults().grab(true, true).applyTo(shellContainer);
	      GridLayoutFactory.fillDefaults().numColumns(3).applyTo(shellContainer);
	      {
	    	  {
		            
		        	      _btnLoadFile = new Button(shellContainer, SWT.PUSH);
		            _btnLoadFile.setText("Load JPEG image");
		            _btnLoadFile.addSelectionListener(new SelectionAdapter() {
		               @Override
		               public void widgetSelected(final SelectionEvent e) {
		                  onLoadImage(parent);
		               }

				
		            });
		            GridDataFactory
	                .fillDefaults()
	                .span(3,1)
	                .applyTo(_btnLoadFile);
		         }
	    	  
	    	  /*
	           * label: Image Quality
	           */
	          _labelImageQuality = new Label(shellContainer, SWT.NONE);
	          _labelImageQuality.setText("Compression");

	          /*
	           * label: Quality Value
	           */
	          _labelImageQualityValue = new Label(shellContainer, SWT.NONE);
	          GridDataFactory
	                .fillDefaults()
	                .align(SWT.BEGINNING, SWT.CENTER)
	                .hint(_pc.convertWidthInCharsToPixels(4), SWT.DEFAULT)
	                .applyTo(_labelImageQualityValue);
	          _labelImageQualityValue.setText("75");

	          /*
	           * scale: JPEG Image Quality
	           */
	          _scaleImageQuality = new Scale(shellContainer, SWT.NONE);
	          _scaleImageQuality.setMinimum(1);
	          _scaleImageQuality.setMaximum(100);
	          _scaleImageQuality.setIncrement(1);
	          _scaleImageQuality.setPageIncrement(10);
	          _scaleImageQuality.setSelection(75);
	          _scaleImageQuality.addSelectionListener(new SelectionAdapter() {
	             @Override
	             public void widgetSelected(final SelectionEvent e) {
	                updateJpegQualityLabel(String.valueOf(_scaleImageQuality.getSelection()));
	             }


	          });
	          GridDataFactory
	                .fillDefaults()
	                .grab(true, false)
	                .applyTo(_scaleImageQuality);
	          
	   	      _btnCompressFile = new Button(shellContainer, SWT.PUSH);
	   	   _btnCompressFile.setText("Compress and export JPEG image");
	   	_btnCompressFile.addSelectionListener(new SelectionAdapter() {
	               @Override
	               public void widgetSelected(final SelectionEvent e) {
	                  onCompressImage(parent);
	               }

			

			
	            });
	            GridDataFactory
              .fillDefaults()
              .span(3,1)
              .applyTo(_btnLoadFile);
	      }
	   }
   
	private static void onCompressImage(Composite parent) {
		 //We load the image
        final Image image = new Image(Display.getCurrent(), jpegAbsolutePath);
        		
        		
        //We compress and save the image
        final ImageLoader loader = new ImageLoader();
        loader.data = new ImageData[] { image.getImageData() };
        loader.compression =  _scaleImageQuality.getSelection();

        int lastIndex = jpegAbsolutePath.lastIndexOf(".");
        String newfilePath = jpegAbsolutePath.substring(0, lastIndex) + "-compressed-"+ _scaleImageQuality.getSelection() + jpegAbsolutePath.substring(lastIndex);
        loader.save(newfilePath, SWT.IMAGE_JPEG);

        MessageDialog.openInformation(Display.getCurrent().getActiveShell(), 
        		"JPEG converted",
        		"Your JPEG image was compressed with the value\n" 
        		+ _scaleImageQuality.getSelection()+
        		"\nand exported to the following location:\n"+
        		newfilePath);
		
	}
	private static void onLoadImage(Composite parent) {
		 FileDialog fd = new FileDialog(Display.getCurrent().getActiveShell(), SWT.OPEN);
	        fd.setText("Open");
	        fd.setFilterPath("C:/");
	        String[] filterExt = { "*.jpg","*.jpeg" };
	        fd.setFilterExtensions(filterExt);
	        jpegAbsolutePath = fd.open();
	        
	       
		
	}
	private static void updateJpegQualityLabel(final String qualityValue) {

	      _labelImageQualityValue.setText(qualityValue);
	   }
}
